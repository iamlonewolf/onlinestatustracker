#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <FS.h>

#ifndef Storage_h
#define Storage_h

class Storage {
    public:
        static const int TOTAL_SSID_LENGTH = 32;
        static const int TOTAL_PASSWORD_LENGTH = 63;

        static const int MIN_ADDRESS_RANGE_SSID = 0;
        static const int MAX_ADDRESS_RANGE_SSID = 32;

        static const int MIN_ADDRESS_RANGE_PASSWORD = 33;
        static const int MAX_ADDRESS_RANGE_PASSWORD = 96;
        
        static const int MAX_BYTE = 255;
        static const size_t MAX_EEPROM_SIZE = 512;

        Storage();
        void initialize();

        void storeCredential(String ssid, String password);
        bool hasCredentials();

        void getSsid(char *ssid);
        void getPassword(char *password);
        void clear(int start, int end);
        void destroyCredentials();

        void setToken(String token);
        String getToken();
        void destroyToken();
    
    private:
        void getValueInMemory(int start, int end, int dataLength, char *data);
};

#endif
