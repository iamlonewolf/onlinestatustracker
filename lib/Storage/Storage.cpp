#include <Storage.h>
#include <EEPROM.h>

Storage::Storage() {
}

void Storage::initialize() {
    EEPROM.begin(Storage::MAX_EEPROM_SIZE);
}

void Storage::clear(int start, int end) {
    Serial.println("Clearing address...");
    for (int i = start; i < end; i++) {
        EEPROM.write(i, 0);
        Serial.printf("Clearing address %d done\n", i);
    }

    if (EEPROM.commit()) {
        Serial.println("Clearing address success.");
    } else {
        Serial.println("Clearing address failed.");
    }
}

void Storage::getValueInMemory(int start, int end, int dataLength, char *data) {
  char TEMP_STORAGE[dataLength];
  bool lastAssignedAddressReached = false;
  for (int memIndex = start, charIndex = 0; memIndex < end; memIndex++, charIndex++) {
      if (!lastAssignedAddressReached) {
          uint8_t memValue = EEPROM.read(memIndex);
          char actualValue = static_cast<char>(memValue);
          if (actualValue != Storage::MAX_BYTE) {
              TEMP_STORAGE[charIndex] = actualValue;
          } else {
            TEMP_STORAGE[charIndex] = '\0';
            lastAssignedAddressReached = true;
          }
          // Serial.printf("INDEX: %d, VALUE: %c\n", charIndex, actualValue);
      }
  }
  strcpy(data, TEMP_STORAGE);
}

void Storage::getSsid(char *val) {
    getValueInMemory(Storage::MIN_ADDRESS_RANGE_SSID, Storage::TOTAL_SSID_LENGTH, Storage::TOTAL_SSID_LENGTH, val);
}

void Storage::getPassword(char *val) {
    getValueInMemory(Storage::MIN_ADDRESS_RANGE_PASSWORD, Storage::MAX_ADDRESS_RANGE_PASSWORD, Storage::TOTAL_PASSWORD_LENGTH, val);
}

void Storage::storeCredential(String ssid, String password) {
    // get ssid stored in flash
    char storedSsid[Storage::TOTAL_SSID_LENGTH];
    getSsid(storedSsid); 
    // get ssid stored in password
    char storedPassword[Storage::TOTAL_PASSWORD_LENGTH];
    getPassword(storedPassword);

    bool shouldChangeSssid = strcmp(storedSsid, ssid.c_str()) != 0;
    bool shouldChangePassword = strcmp(storedPassword, password.c_str()) != 0;

    Serial.printf("should change ssid: %s\n", shouldChangeSssid ? "yes" : "no");
    Serial.printf("should change password: %s\n", shouldChangePassword ? "yes" : "no");

    if (shouldChangeSssid) {
        // clear ssid address in memory
        clear(Storage::MIN_ADDRESS_RANGE_SSID, Storage::MAX_ADDRESS_RANGE_SSID + 1);
        Serial.printf("storing ssid = %s\n", ssid.c_str());
        for (unsigned int i = 0; i < ssid.length(); i++) {
            EEPROM.write(i, static_cast<char>(ssid[i]));
        }
    }
    if (shouldChangePassword) {
        clear(Storage::MIN_ADDRESS_RANGE_PASSWORD + 1, Storage::MAX_ADDRESS_RANGE_PASSWORD + 1);
        Serial.printf("storing password = %s\n", password.c_str());
        for (unsigned int i = 0; i < password.length(); i++) {
            EEPROM.write(Storage::MIN_ADDRESS_RANGE_PASSWORD + i, static_cast<char>(password[i]));
        }
    }
    if (shouldChangeSssid || shouldChangePassword) {
        if (EEPROM.commit()) {
            Serial.println("credential is updated.");
        } else {
            Serial.println("failed storing credential.");
        }
    }
}

bool Storage::hasCredentials() {
    char ssid[Storage::TOTAL_SSID_LENGTH];
    getSsid(ssid);
    char password[Storage::TOTAL_PASSWORD_LENGTH];
    getPassword(password);
    return strlen(ssid) > 0 && strlen(password) > 0;
}

void Storage::destroyCredentials() {
    clear(Storage::MIN_ADDRESS_RANGE_SSID, Storage::MAX_ADDRESS_RANGE_SSID + 1);
    clear(Storage::MIN_ADDRESS_RANGE_PASSWORD + 1, Storage::MAX_ADDRESS_RANGE_PASSWORD + 1);
}

void Storage::setToken(String token) {
    Serial.println("saving token..");
    File file = SPIFFS.open("/token", "w");
    if (!file) {
        Serial.println("unabled to save token in file");
    } else {
        Serial.println("token is saved.");
        file.print(token);
        file.close();
    }
}

String Storage::getToken() {
    String token = "";
    File file = SPIFFS.open("/token", "r");
    if (!file) {
        Serial.println("unabled to open token in file");
        return token;
    } else {
        token = file.readString();
        file.close();
    }
    Serial.println();
    Serial.print("retrieved token = ");
    Serial.print(token);
    Serial.println();
    return token;
};

void Storage::destroyToken() {
    if (SPIFFS.remove("/token")) {
        Serial.println("token destroyed.");
    } else {
        Serial.println("failed to destroy token.");
    }
};