#include <ServiceApi.h>

wl_status_t ServiceApi::_status;
const String ServiceApi::ENDPOINT_ACCOUNT = "/account";
const String ServiceApi::ENDPOINT_IDENTITY_LOGIN = "/identity/login";
const String ServiceApi::ENDPOINT_CONNECTION_STATE_ACTIVE = "/account/connectionState/active";
const String ServiceApi::ENDPOINT_CONNECTION_STATE_INACTIVE = "/account/connectionState/inactive";
const String ServiceApi::ENDPOINT_CONNECTION_STATE_AWAY = "/account/connectionState/away";
const String ServiceApi::ENDPOINT_CONNECTION_STATE_BUSY = "/account/connectionState/busy";
const String ServiceApi::ENDPOINT_CONNECTION_STATE_OFF = "/account/connectionState/off";


ServiceApi::ServiceApi() {
}

void ServiceApi::request(String endpoint, 
        const char * type, 
        String payload, 
        std::function<void(HTTPClient*, int httpCode)> cb,
        std::function<void(int, String)> error
    ) {
    if (getConnectionStatus() != WL_CONNECTED) {
        error(-1, "unavailable");
        return;
    }

    const char * headerKeys[] = {"authorization"} ;
    const size_t numberOfHeaders = 1;
    WiFiClient client;
    HTTPClient http;
    String token = _storage.getToken();
    http.collectHeaders(headerKeys, numberOfHeaders);
    Serial.printf("initialize request to %s\n", endpoint.c_str());
    Serial.printf("request payload %s\n", payload.c_str());
    http.useHTTP10(true);
    if (http.begin(client, endpoint)) {
        http.addHeader("Content-Type", "application/json");
        if (!token.isEmpty()) {
            http.addHeader("authorization", token);
        }
        http.setTimeout(Timey::SECOND_3);
        int httpCode = http.sendRequest(type, payload);  
        Serial.println("===BEGIN HTTP===");
        Serial.printf("HTTP CODE=%d\n", httpCode);
        Serial.print("HTTP RESPONSE=\n");
        Serial.println(http.getString());
        if (httpCode <= 0) {
            error(httpCode, "");
        } else {
            cb(&http, httpCode);
        }
        Serial.println("===END HTTP===");
        http.end();
    }
}

void ServiceApi::get(String endpoint, 
        String payload, 
        std::function<void(HTTPClient*, int httpCode)> cb,
        std::function<void(int, String)> error
    ) {
    request(endpoint, "GET", payload, cb, error);
}

void ServiceApi::post(String endpoint, 
    String payload, 
    std::function<void(HTTPClient*, int httpCode)> cb,
    std::function<void(int, String)> error) {
    request(endpoint, "POST", payload, cb, error);
}
