#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <Timey.h>
#include <Storage.h>

#ifndef ServiceApi_H
#define ServiceApi_H

class ServiceApi {
    public:
        ServiceApi();
        static const String ENDPOINT_ACCOUNT;
        static const String ENDPOINT_IDENTITY_LOGIN;
        static const String ENDPOINT_CONNECTION_STATE_ACTIVE;
        static const String ENDPOINT_CONNECTION_STATE_INACTIVE;
        static const String ENDPOINT_CONNECTION_STATE_AWAY;
        static const String ENDPOINT_CONNECTION_STATE_BUSY;
        static const String ENDPOINT_CONNECTION_STATE_OFF;
        void request(String endpoint, 
            const char * type, 
            String payload, 
            std::function<void(HTTPClient*, int httpCode)> cb,
            std::function<void(int, String)> error
        );
        void get(String endpoint, 
            String payload, 
            std::function<void(HTTPClient*, int httpCode)> cb,
            std::function<void(int, String)> error
        );
        void post(String endpoint, 
            String payload, 
            std::function<void(HTTPClient*, int httpCode)> cb,
            std::function<void(int, String)> error
        );

        static void setConnectionStatus(wl_status_t status) {
            _status = status;
        };

        static wl_status_t getConnectionStatus() {
            return _status;
        };

    private:
        static wl_status_t _status;
        Storage _storage;
};

#endif