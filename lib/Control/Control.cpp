#include <Control.h>

Control::Control() {
}

void Control::initialize(std::vector<ControlSetttings> controls) {
    _controls = controls;
    get([](ControlSetttings* settings) {
        pinMode(settings->pin, settings->mode);
    });
}

void Control::get(std::function<void(ControlSetttings*)> cb) {
    for(std::vector<ControlSetttings>::iterator it = _controls.begin(); it != _controls.end(); ++it) {
        cb(&*it);
    }
}

void Control::getPin(uint8_t pin, std::function<void(ControlSetttings*)> cb) {
    get([pin, cb](ControlSetttings* settings) {
        if (settings->pin == pin) {
            cb(settings);
        }
    });
}

void Control::toggle(uint8_t pin, uint8_t mode) {
    getPin(pin, [mode](ControlSetttings* settings) {
        digitalWrite(settings->pin, mode);
    });
}

void Control::toggleAllExcept(uint8_t pin, uint8_t mode) {
    get([mode, pin](ControlSetttings* settings) {
        if (settings->pin == pin) {
            digitalWrite(settings->pin, mode);
        } else {
            digitalWrite(settings->pin, LOW);
        }
    });
}

void Control::toggleOn(uint8_t pin) {
    getPin(pin, [](ControlSetttings* settings) {
        digitalWrite(settings->pin, HIGH);
    });
}

void Control::toggleOff(uint8_t pin) {
    getPin(pin, [](ControlSetttings* settings) {
        digitalWrite(settings->pin, LOW);
    });
}

void Control::toggleAllOn() {
    get([](ControlSetttings* settings) {
        digitalWrite(settings->pin, HIGH);
    });
}

void Control::toggleAllOff() {
    get([](ControlSetttings* settings) {
        digitalWrite(settings->pin, LOW);
    });
}
