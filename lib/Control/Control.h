#include <Arduino.h>
#include <Timey.h>

#ifndef Control_h
#define Control_h

struct ControlSetttings {
    uint8_t pin;
    uint8_t mode;
};

class Control {
    public:
        Control();
        void initialize(std::vector<ControlSetttings> leds);
        void toggleAllOn();
        void toggleAllOff();
        void toggleOn();
        void toggleAllExcept(uint8_t pin, uint8_t mode);
        void toggle(uint8_t pin, uint8_t mode);
        void toggleOn(uint8_t pin);
        void toggleOff(uint8_t pin);
        void blink(uint8_t pin, unsigned long interval);
        void get(std::function<void(ControlSetttings*)> cb);
        void getPin(uint8_t pin, std::function<void(ControlSetttings*)> cb);
        
    private:
        std::vector<ControlSetttings> _controls;
};

#endif