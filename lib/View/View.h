#include <Arduino.h>
#include <U8g2lib.h>

#ifndef View_h
#define View_h

class View {
    public:
        U8G2* _u8g2;
        View(U8G2* u8g2) {
            _u8g2 = u8g2;
        };

        void initialize(U8G2* u8g2);
        static const u8g2_uint_t FONT_SIZE_8 = 8;
        static const u8g2_uint_t FONT_SIZE_10 = 10;
        static const u8g2_uint_t FONT_SIZE_12 = 12;
        static const u8g2_uint_t FONT_SIZE_14 = 14;
        static const u8g2_uint_t FONT_SIZE_18 = 18;
        u8g2_uint_t getTextHorizontalCenter(const char* text);
        u8g2_uint_t getTextVerticalCenter(u8g2_uint_t height);

        void drawBorder();
        void welcomeMessageText(bool show = true);
        void activeText(bool show = true);
        void inactiveText(bool show = true);
        void awayText(bool show = true);
        void busyText(bool show = true);
        void offlineText(bool show = true);
        void scanningText(bool show = true);
        void sendingLogStatusActiveText(bool show = true);
        void sendingLogStatusInactiveText(bool show = true);
        void logoutText();
        void loggingInText();
        void wifiConnectingText();
        void wifiConnectionSuccessText();
        void wifiConnectionFailedText();
        void watchState();
};

#endif
