#include <Arduino.h>
#include <View.h>

// View::View(U8G2* u8g2) {
// }

void View::initialize(U8G2* u8g2) {
  _u8g2 = u8g2;
}

u8g2_uint_t View::getTextHorizontalCenter(const char* text) {
  return _u8g2->getDisplayWidth() / 2 - _u8g2->getStrWidth(text) / 2;
}

u8g2_uint_t View::getTextVerticalCenter(u8g2_uint_t height) {
  return _u8g2->getDisplayHeight() / 2 + height / 2;
}

void View::welcomeMessageText(bool show) {
    if (!show) {
      return;
    }
    char *text;
    asprintf(&text, "%s", "AFK Tracker");
    _u8g2->clearBuffer();
    _u8g2->setFont(u8g2_font_luBS12_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_12), text);
    _u8g2->sendBuffer();		
    free(text);
}

void View::activeText(bool show) {
    if (!show) {
      return;
    }
    char *text;
    asprintf(&text, "%s", "Active");   
    _u8g2->setFont(u8g2_font_luBS18_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_18), text);
    free(text);
}

void View::inactiveText(bool show) {
    if (!show) {
      return;
    }
    char *text;
    asprintf(&text, "%s", "Inactive");
    _u8g2->setFont(u8g2_font_luBS18_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_18), text);
    free(text);
}

void View::awayText(bool show) {
    if (!show) {
      return;
    }
    char *text;
    asprintf(&text, "%s", "Away");
    _u8g2->setFont(u8g2_font_luBS18_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_18), text);
    free(text);
}

void View::busyText(bool show) {
    if (!show) {
      return;
    }
    char *text;
    asprintf(&text, "%s", "Busy");
    _u8g2->setFont(u8g2_font_luBS18_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_18), text);
    free(text);
}

void View::offlineText(bool show) {
    if (!show) {
      return;
    }
    char *text;
    asprintf(&text, "%s", "Offline");
    _u8g2->setFont(u8g2_font_luBS18_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_18), text);
    free(text);
}

void View::scanningText(bool show) {
    if (!show) {
      return;
    }
    char *text;
    asprintf(&text, "%s", "Scanning..");   
    _u8g2->setFont(u8g2_font_luBS14_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_14), text);
    free(text);
}

void View::sendingLogStatusActiveText(bool show) {
    if (!show) {
      return;
    }
    char *text;
    asprintf(&text, "%s", "Sending active..");   
    _u8g2->setFont(u8g2_font_luBS08_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_8), text);
    free(text);
}

void View::logoutText() {
    char *text;
    asprintf(&text, "%s", "Logging out..");   
    _u8g2->setFont(u8g2_font_luBS08_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_8), text);
    free(text);
}

void View::loggingInText() {
    char *text;
    asprintf(&text, "%s", "Logging in..");   
    _u8g2->setFont(u8g2_font_luBS08_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_8), text);
    free(text);
}

void View::sendingLogStatusInactiveText(bool show) {
    if (!show) {
      return;
    }
    char *text1;
    char *text2;
    asprintf(&text1, "%s", "Sending inactive");   
    asprintf(&text2, "%s", "status..");   
    _u8g2->setFont(u8g2_font_luBS10_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text1), 15, text1);
    _u8g2->drawStr(getTextHorizontalCenter(text2), 30, text2);
    free(text1);
    free(text2);
}

void View::wifiConnectingText() {
    char *text;
    asprintf(&text, "%s", "Connecting..");   
    _u8g2->clearBuffer();
    _u8g2->setFont(u8g2_font_luBS10_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_10), text);
    _u8g2->sendBuffer();
    free(text);
}

void View::wifiConnectionSuccessText() {
    char *text;
    asprintf(&text, "%s", "Connected.");   
    _u8g2->clearBuffer();
    _u8g2->setFont(u8g2_font_luBS10_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_10), text);
    _u8g2->sendBuffer();
    free(text);
}

void View::wifiConnectionFailedText() {
    char *text;
    asprintf(&text, "%s", "Not connected.");   
    _u8g2->clearBuffer();
    _u8g2->setFont(u8g2_font_luBS10_tr);
    _u8g2->drawStr(getTextHorizontalCenter(text), getTextVerticalCenter(FONT_SIZE_10), text);
    _u8g2->sendBuffer();
    free(text);
}

void View::drawBorder() {
  _u8g2->drawRFrame(0, 0, _u8g2->getDisplayWidth(), _u8g2->getDisplayHeight(), 0);
}
