#include <UltraSonic.h>

UltraSonic::UltraSonic(uint8_t triggerPin, uint8_t echoPin) {
    _triggerPin = triggerPin;
    _echoPin = echoPin;
    initialize();
}

void UltraSonic::initialize() {
    std::vector<ControlSetttings> controls;
    ControlSetttings trigger;
    trigger.pin = _triggerPin;
    trigger.mode = OUTPUT;
    controls.push_back(trigger);

    ControlSetttings echo;
    echo.pin = _echoPin;
    echo.mode = INPUT;
    controls.push_back(echo);

    _control.initialize(controls);
}

void UltraSonic::setMaxRange(unsigned int rangeInInches) {
    maxRange = rangeInInches;
}

unsigned int UltraSonic::getMaxRange() {
    return maxRange;
}

void UltraSonic::detect() {
    digitalWrite(_triggerPin, LOW);
    _control.toggleOff(_triggerPin);
    delayMicroseconds(2);
    _control.toggleOn(_triggerPin);
    delayMicroseconds(10);
    _control.toggleOff(_triggerPin);

    // ultrasonic sensor trigger and echo
    DURATION = pulseIn(_echoPin, HIGH);
    DISTANCE_CM = DURATION * SOUND_VELOCITY / 2;
    DISTANCE_IN = DISTANCE_CM * CM_TO_INCH;
    distanceInfo.cm = DISTANCE_CM;
    distanceInfo.in = static_cast<int>(DISTANCE_IN);

    // Serial.printf("detected target distance : %d\n", DETECTED_DISTANCE);    
    /* 
    if (DETECTED_DISTANCE > INVALID_RANGE) {
        if (millis() > invalidRangeIntervalMills + INVALID_RANGE_DETECTION_INTERVAL) {
            validRange = false;
            invalidRangeIntervalMills = millis();
        }
    } else {
        validRange = true;
    }

    if (DETECTED_DISTANCE > getTargetDistanceRange()) {
        if (millis() > awayIntervalResetMills + getDetectionInterval()) {
            distanceWithinTarget = false;
            awayIntervalResetMills = millis();
        }
    } else {
        if (DETECTED_DISTANCE < getTargetDistanceRange()) {
            distanceWithinTarget = true;
        }        
    }
     */
}


DistanceInfo UltraSonic::getDistanceInfo() {
    return distanceInfo;
}

unsigned int UltraSonic::getInch() {
    return getDistanceInfo().in;
}

unsigned int UltraSonic::getCentimeters() {
    return getDistanceInfo().cm;
}
