#include <Arduino.h>
#include <Control.h>
#include <Timey.h>

#ifndef UltraSonic_h
#define UltraSonic_h

// #define TRIGGER_PIN  12
// #define ECHO_PIN 14
#define SOUND_VELOCITY 0.034
#define CM_TO_INCH 0.393701

struct DistanceInfo {
    unsigned int cm;
    unsigned int in;
};

class UltraSonic {
    public:
        UltraSonic(uint8_t triggerPin, uint8_t echoPin);
        const int MAX_DISTANCE_DETECTION_RANGE = 19;
        uint8_t _triggerPin;
        uint8_t _echoPin;

        DistanceInfo getDistanceInfo();
        void detect();
        unsigned int getInch();
        unsigned int getCentimeters();
 
        void setMaxRange(unsigned int rangeInInches);
        unsigned int getMaxRange();

    private:
        void initialize();
        Control _control;
        DistanceInfo distanceInfo;
        const int INVALID_RANGE_DETECTION_INTERVAL = 30000;
        const int INVALID_RANGE = 900;
        long DURATION;
        float DISTANCE_CM;
        float DISTANCE_IN;

        int invalidVal = 900;
        int detectionInterval = 60000;
        unsigned int maxRange = 0; // 19 test value

        unsigned long awayIntervalResetMills = 0;
        unsigned long invalidRangeIntervalMills = 0;
        bool distanceWithinTarget = true;
        bool validRange = true;

        TimeyBoolCb rangeCheckInterval = Timey::createToggle();
};

#endif
