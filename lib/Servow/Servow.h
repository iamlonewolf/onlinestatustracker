#include <Arduino.h>
#include <Servo.h>

#ifndef Servow_h
#define Servow_h

class Servow {
    public:
        Servow(uint8_t pin);

        static const int DEGREE_0 = 0;
        static const int DEGREE_90 = 90;
        static const int DEGREE_180 = 180;
        static const int DEGREE_360 = 360;
        static const int MAX_DEGREE = Servow::DEGREE_180;
        static const int ROTATE_1 = 1;
        static const int ROTATE_2 = 2;
        static const int ROTATE_3 = 3;
        static const int ROTATE_4 = 4;
        static const int ROTATE_5 = 5;
        static const int ROTATE_6 = 6;
        static const int ROTATE_7 = 7;
        
        void rotate(int degree);
        void setDegree(int degree);
        int getDegree();
        void setNumRotation(int rotation);
        int getNumRotation();
        int getDegreeRotation();

    private:
        Servo _servow;
        int _degree = MAX_DEGREE;
        int _numRotation;
};

#endif
