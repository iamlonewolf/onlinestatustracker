#include <Servow.h>

Servow::Servow(uint8_t pin) {
    _servow.attach(pin);
}

void Servow::rotate(int degree) {
    _servow.write(degree);
}

void Servow::setDegree(int degree) {
    _degree = degree;
}

int Servow::getDegree() {
    return _degree;
}

void Servow::setNumRotation(int numRotation) {
    _numRotation = numRotation;
}

int Servow::getNumRotation() {
    return _numRotation;
}

int Servow::getDegreeRotation() {
    return static_cast<int>(getDegree() / getNumRotation());
}