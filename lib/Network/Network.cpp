#include <Network.h>

Network::Network() {
    gotIpEventHandler = WiFi.onStationModeGotIP([this](const WiFiEventStationModeGotIP& event){
        Serial.println("station connected ip:");
        Serial.print(WiFi.localIP());
        WIFI_CONNECTION_STATUS = WL_CONNECTED;
    });

    disconnectedEventHandler = WiFi.onStationModeDisconnected([this](const WiFiEventStationModeDisconnected& event) {
        Serial.println("station discconnected");
        WIFI_CONNECTION_STATUS = WL_DISCONNECTED;
    });
}

void Network::setConnectionRetry(int retry) {
  _retry = retry;
} 

unsigned int Network::getConnectionRetry() {
  return _retry;
}

void Network::initializeConnection(char *ssid, char *password) {
  connect(ssid, password);
  unsigned int retryConnectionCounter = 0;
  while ((WIFI_CONNECTION_STATUS = WiFi.status()) != WL_CONNECTED && 
    WIFI_CONNECTION_STATUS != WL_NO_SSID_AVAIL &&
    retryConnectionCounter < getConnectionRetry()) {
    Serial.print(".");
    delay(Network::WIFI_CONNECTION_DELAY);
    retryConnectionCounter++;
  }
  Serial.println();
}

void Network::connect(char *ssid, char *password) {
    Serial.print("\nConnecting");
    WiFi.begin(ssid, password);
}


void Network::disconnect() {
  WiFi.disconnect();
}

String Network::getActiveNetworkData() {
  String json = "[";
  int numOfNetworks = WiFi.scanComplete();
  if(numOfNetworks == -2){
    WiFi.scanNetworks(true);
  } else if(numOfNetworks) {
    for (int i = 0; i < numOfNetworks; ++i) {
      if(i) json += ",";
      json += "{";
      json += "\"rssi\":" + String(WiFi.RSSI(i));
      json += ",\"ssid\":\"" + WiFi.SSID(i)+"\"";
      json += ",\"bssid\":\"" + WiFi.BSSIDstr(i)+"\"";
      json += ",\"channel\":" + String(WiFi.channel(i));
      json += ",\"secure\":" + String(WiFi.encryptionType(i));
      json += ",\"hidden\":" +String(WiFi.isHidden(i)? "true" : "false");
      json += "}";
    }
    WiFi.scanDelete();
    if(WiFi.scanComplete() == -2){
      WiFi.scanNetworks(true);
    }
  }
  json += "]";
  return json;
}

bool Network::isConnected() {
  return WIFI_CONNECTION_STATUS == WL_CONNECTED;
}