#include <Arduino.h>
#include <ESP8266WiFi.h>

#ifndef Network_h
#define Network_h

class Network {
    public:
        Network();
    
        wl_status_t WIFI_CONNECTION_STATUS; // wifi connection status
        static const int DEFAULT_RETRY = 15;
        static const int WIFI_CONNECTION_DELAY = 1000;
        void initializeConnection(char *ssid, char *password);
        void connect(char *ssid, char *password);
        void disconnect();
        String getActiveNetworkData();

        void setConnectionRetry(int retry = Network::DEFAULT_RETRY);
        unsigned int getConnectionRetry();
        bool isConnected();
    private:
        unsigned int _retry = 0;
        WiFiEventHandler gotIpEventHandler, disconnectedEventHandler;
};

#endif
