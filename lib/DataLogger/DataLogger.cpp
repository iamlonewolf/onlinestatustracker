
#include <DataLogger.h>

DataLogger::DataLogger() {
}

void DataLogger::postStatusActive() {
    Serial.println("sending post active status started");
    const String endpoint = DevConfig::getApiUrl() + ServiceApi::ENDPOINT_CONNECTION_STATE_ACTIVE;
    String payload;
    StaticJsonDocument<100> doc;
    doc["deviceId"] = DevConfig::DEVICE_ID;
    serializeJson(doc, payload);

     _service.post(endpoint, payload,  [](HTTPClient *http, int httpCode) {
        Serial.println("sending post ACTIVE status success");
    }, [this](int httpCode, String error) {
        Serial.println("sending post ACTIVE status failed");
    });
}

void DataLogger::postStatusInactive() {
    Serial.println("sending post inactive status started");
    const String endpoint = DevConfig::getApiUrl() + ServiceApi::ENDPOINT_CONNECTION_STATE_INACTIVE;
    String payload;
    StaticJsonDocument<100> doc;
    doc["deviceId"] = DevConfig::DEVICE_ID;
    serializeJson(doc, payload);
     _service.post(endpoint, payload,  [](HTTPClient *http, int httpCode) {
        Serial.println("sending post INACTIVE status success");
    }, [this](int httpCode, String error) {
        Serial.println("sending post INACTIVE status failed");
    });
}

void DataLogger::postStatusAway() {
    Serial.println("sending post away status started");
    const String endpoint = DevConfig::getApiUrl() + ServiceApi::ENDPOINT_CONNECTION_STATE_AWAY;
    String payload;
    StaticJsonDocument<100> doc;
    doc["deviceId"] = DevConfig::DEVICE_ID;
    serializeJson(doc, payload);
     _service.post(endpoint, payload,  [](HTTPClient *http, int httpCode) {
        Serial.println("sending post AWAY status success");
    }, [this](int httpCode, String error) {
        Serial.println("sending post AWAY status failed");
    });
}

void DataLogger::postStatusBusy() {
    Serial.println("sending post busy status started");
    const String endpoint = DevConfig::getApiUrl() + ServiceApi::ENDPOINT_CONNECTION_STATE_BUSY;
    String payload;
    StaticJsonDocument<100> doc;
    doc["deviceId"] = DevConfig::DEVICE_ID;
    serializeJson(doc, payload);
     _service.post(endpoint, payload,  [](HTTPClient *http, int httpCode) {
        Serial.println("sending post BUSY status success");
    }, [this](int httpCode, String error) {
        Serial.println("sending post BUSY status failed");
    });
}

void DataLogger::postStatusOff() {
    Serial.println("sending post off");
    const String endpoint = DevConfig::getApiUrl() + ServiceApi::ENDPOINT_CONNECTION_STATE_OFF;
    String payload;
    StaticJsonDocument<100> doc;
    doc["deviceId"] = DevConfig::DEVICE_ID;
    serializeJson(doc, payload);
     _service.post(endpoint, payload,  [](HTTPClient *http, int httpCode) {
        Serial.println("sending post OFF status success");
    }, [this](int httpCode, String error) {
        Serial.println("sending post OFF status failed");
    });
}

// void DataLogger::getAccount() {
//     Serial.println("sending post INACTIVE status started");
//     const String endpoint = DevConfig::getApiUrl() + ServiceApi::ENDPOINT_ACCOUNT "?accountId=abc123456789&deviceId=abc123456789";
//     const String payload = "";
//     _service.get(endpoint, payload, [](HTTPClient *http, int httpCode) {
//         Serial.println("sending post INACTIVE status success");
//         StaticJsonDocument<70> doc;
//         deserializeJson(doc, http->getStream());
//         Serial.println(doc["message"].as<String>());
//     }, [](int httpCode, String error) {
//         Serial.println("sending post INACTIVE status failed");
//     });
// }
