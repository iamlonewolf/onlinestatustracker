#include <Arduino.h>
#include <ArduinoJson.h>
#include <ServiceApi.h>
#include <DevConfig.h>

#ifndef DataLogger_h
#define DataLogger_h

class DataLogger {
    public:
        DataLogger();
        void postStatusActive();
        void postStatusInactive();
        void postStatusAway();
        void postStatusBusy();
        void postStatusOff();
    private:
        ServiceApi _service;
};

#endif