#include <MyServer.h>

MyServer::MyServer() {
    _storage.initialize();
    _network.setConnectionRetry(15);
}

String MyServer::processor(const String& var, String content) {
    if (var == "API_URL") {
        return DevConfig::getApiUrl();
    }

    if (var == "WIFI_CONNECTED") {
        return _network.isConnected() ? "true" : "false";
    }

    if (content) {
        return content;
    }
    return String();
}

/***
 * Globe WiFI credentials
 * SSID: GlobeAtHome_0B3D0
 * PASSWORD: JH3DAA9AHEF
 * 
 * PLDT WiFI credentials
 * SSID: PLDTHOMEFIBRc1d68
 * PASSWORD: PLDTWIFI9w3xu
 ***/
void MyServer::initialize(std::function<void(MyServerStatus)> cb) {
    WiFi.mode(WIFI_AP_STA);
    WiFi.softAPConfig(_apId, _apId, _subNetMask);
    WiFi.softAP(MyServer::SSID);
    
    if (_storage.hasCredentials()) {
        cb(MyServerStatus::CONNECTING);
        connect();
        cb(_network.isConnected() ? MyServerStatus::SUCCESS : MyServerStatus::FAILED);
    } else {
        cb(MyServerStatus::UNAVAILABLE);
    }
}

void MyServer::connect(bool async) {
    if (_storage.hasCredentials()) {
        char ssid[Storage::TOTAL_SSID_LENGTH];
        char password[Storage::TOTAL_PASSWORD_LENGTH];
        credentials(ssid, password);
        if (!async) {
            _network.initializeConnection(ssid, password);
        } else {
            _network.disconnect();
            _network.connect(ssid, password);
        }
    }
}

void MyServer::credentials(char* ssid, char *password) {
    _storage.getSsid(ssid);
    Serial.printf("current ssid: %s\n", ssid);

    _storage.getPassword(password);
    Serial.printf("current password: %s\n", password);
    Serial.printf("credentials found: %s\n", _storage.hasCredentials() ? "yes" : "no");
}

void MyServer::handleMainContent(AsyncWebServerRequest *request) {
    if (_network.WIFI_CONNECTION_STATUS != WL_CONNECTED) {
        request->redirect("/networks");
        return;
    }
    
    request->send(SPIFFS,  "/index.html", String(), false, [this](const String& var) -> String {
        return processor(var, MAIN_CONTENT);
    });
}

void MyServer::handlNetworkContent(AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/index.html", String(), false, [this](const String& var) -> String {
        return processor(var, NETWORK_LIST_CONTENT);
    });
}

void MyServer::handleStatusListContent(AsyncWebServerRequest *request) {
    request->send(SPIFFS,  "/index.html", String(), false, [this](const String& var) -> String {
        return processor(var, STATUS_LIST_CONTENT);
    });
}

void MyServer::handleNetworkLogin(AsyncWebServerRequest *request) {
    request->send(SPIFFS,  "/index.html", String(), false, [this, request](const String& var) -> String {
        if (var == "SSID") {
            AsyncWebParameter* p = request->getParam("ssid");
            return p->value().c_str();
        }
        return processor(var, NETWORK_LOGIN_CONTENT);
    });
}

void MyServer::handleAccountLogin(AsyncWebServerRequest *request) {
    request->send(SPIFFS,  "/index.html", String(), false, [this, request](const String& var) -> String {
        return processor(var, ACCOUNT_LOGIN_CONTENT);
    });
}

void MyServer::handleAccountLogout(AsyncWebServerRequest *request) {
    State::setAppState(AppStateMode::LOGOUT);
    if (request->hasParam("redirect")) {
        AsyncWebParameter* redirectParam = request->getParam("redirect", false);
        const String redirect = redirectParam->value();
        request->redirect(redirect);
        return;
    }
    request->redirect("/main");
}

void MyServer::handleAccountProfile(AsyncWebServerRequest *request) {
    if (!_network.isConnected()) {
        request->redirect("/networks");
        return;
    }
    request->send(SPIFFS,  "/index.html", String(), false, [this, request](const String& var) -> String {
        return processor(var, ACCOUNT_PROFILE_CONTENT);
    });
}

void MyServer::handlePOSTToken(AsyncWebServerRequest *request) {
    AsyncWebParameter* tokenParam = request->getParam("token", true);
    if (tokenParam->value().isEmpty()) {
        request->send(405, "text/json", "{\"message\": \"error\"}");
        return;
    }
    _storage.setToken(tokenParam->value());
    request->send(200, "text/json", "{\"message\": \"ok\"}");
}

void MyServer::handleGETToken(AsyncWebServerRequest *request) {
    AsyncWebServerResponse *response = request->beginResponse(200, "text/json", "{\"message\": \"ok\"}");
    response->addHeader("authorization", _storage.getToken());
    request->send(response);
}

void MyServer::handleLoginWithToken(AsyncWebServerRequest *request) {
    State::setAppState(AppStateMode::LOG_ACTIVE);
    handlePOSTToken(request);
}

void MyServer::initializeServer() {
    _dnsServer.start(MyServer::DNS_PORT, "*", _apId);

    _server.on("/bootstrap.min.css", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS, "/bootstrap.min.css", "text/css");
    });

    _server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS,  "/style.css", "text/css");
    });

    _server.on("/app.js", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS,  "/app.js", "text/javascript");
    });

    _server.on("/validateNetworkLogin", HTTP_POST, [this](AsyncWebServerRequest *request) {
        AsyncWebParameter* ssidParam = request->getParam("ssid", true);
        AsyncWebParameter* passwordParam = request->getParam("password", true);
        String ssid = ssidParam->value();
        String password = passwordParam->value();
        
        Serial.printf("SSID: %s, LENGTH %d\n", ssid.c_str(), ssid.length());
        Serial.printf("PASSWORD: %s, LENGTH %d\n", password.c_str(), password.length());

        if (request->hasParam("ssid", true) && request->hasParam("password", true)) {
            delay(Timey::SECOND_1);
            _storage.storeCredential(ssid, password);
        }

        request->send(200, "text/json", "{\"message\": \"ok\"}");
    });

    _server.on("/validateAccountLogin", HTTP_POST, [this](AsyncWebServerRequest *request) {
        AsyncWebParameter* emailParam = request->getParam("email", true);
        AsyncWebParameter* passwordParam = request->getParam("password", true);
        email = emailParam->value();
        password = passwordParam->value();
        State::setAppState(AppStateMode::ACCOUNT_LOGIN);
        request->send(200, "text/json", "{\"message\": \"ok\"}");
    });

    _server.on("/checkAccountLoginStatus", HTTP_GET, [this](AsyncWebServerRequest *request) {
        request->send(200, "text/json", "{\"message\": \""+ State::getAccountLoginStateToString() + "\"}");
    });

    _server.on("/token", HTTP_POST, [this](AsyncWebServerRequest *request) {
        handlePOSTToken(request);
    });

    _server.on("/token", HTTP_GET, [this](AsyncWebServerRequest *request) {
        handleGETToken(request);
    });

    _server.on("/login/token", HTTP_POST, [this](AsyncWebServerRequest *request) {
        handleLoginWithToken(request);
    });
    
    _server.on("/configured", HTTP_GET, [this](AsyncWebServerRequest *request) {
        request->send(SPIFFS,  "/index.html", String(), false, [this](const String& var) -> String {
            return processor(var, NETWORK_CONFIGURED_CONTENT);
        });
    });

    _server.on("/main", HTTP_GET, [this](AsyncWebServerRequest *request) {
        handleMainContent(request);
    });

    _server.on("/setStatus", HTTP_GET, [this](AsyncWebServerRequest *request) {
        handleStatusListContent(request);
    });

    _server.on("/scan", HTTP_GET, [this](AsyncWebServerRequest *request) {
        String str = _network.getActiveNetworkData();
        request->send(200, "text/json", str);
    });

    _server.on("/networks", HTTP_GET, [this](AsyncWebServerRequest *request) {
        handlNetworkContent(request);
    });

    _server.on("/networkLogin", HTTP_GET, [this](AsyncWebServerRequest *request) {
        handleNetworkLogin(request);
    });

    _server.on("/accountLogin", HTTP_GET, [this](AsyncWebServerRequest *request) {
        handleAccountLogin(request);
    });

    _server.on("/accountLogout", HTTP_GET, [this](AsyncWebServerRequest *request) {
        handleAccountLogout(request);
    });

    _server.on("/accountProfile", HTTP_GET, [this](AsyncWebServerRequest *request) {
        handleAccountProfile(request);
    });

    _server.on("/checkLogin", HTTP_GET, [this](AsyncWebServerRequest *request) {
        request->redirect(getRedirectUrl("/main"));
    });

    _server.on("/checkNetworkLoginStatus", HTTP_GET, [this](AsyncWebServerRequest *request) {
        if (_network.isConnected()) {
            request->send(200, "text/json", "{\"status\": \"success\"}");
        } else {
            request->send(401, "text/json", "{\"status\": \"failed\"}");
        }
    });

    _server.on("/status", HTTP_POST, [this](AsyncWebServerRequest *request) {
        if (request->hasParam("status", true)) {
            AsyncWebParameter* statusParam = request->getParam("status", true);
            String status = statusParam->value();
            if (status == CONNECTION_STATE_LABEL_BUSY || 
                status == CONNECTION_STATE_LABEL_AWAY || 
                status == CONNECTION_STATE_LABEL_OFF ) {
                State::setUserInitiated(true);
            }

            if (status == CONNECTION_STATE_LABEL_ACTIVE) {
                Serial.println("changing status to active");
                State::setUserInitiated(false);
                State::setAppState(AppStateMode::LOG_ACTIVE);
            }

            if (status == CONNECTION_STATE_LABEL_BUSY) {
                Serial.println("changing status to busy");
                State::setAppState(AppStateMode::LOG_BUSY);
            }
            if (status == CONNECTION_STATE_LABEL_AWAY) {
                Serial.println("changing status to away");                
                State::setAppState(AppStateMode::LOG_AWAY);
            }
            if (status == CONNECTION_STATE_LABEL_OFF) {
                Serial.println("changing status to offline");
                State::setAppState(AppStateMode::LOG_OFF);
            }
        }
        request->send(200, "text/json", "{\"status\": \"ok\"}");
    });

    _server.on("/status", HTTP_GET, [this](AsyncWebServerRequest *request) {
        request->send(200, "text/json", "{\"status\": \"" + State::getAppStateText() + "\"}");
    });

    _server.on("/connect", HTTP_GET, [this](AsyncWebServerRequest *request) {
        connect(true);
        request->send(200, "text/json", "{\"status\": \"ok\"}");
    });

    _server.on("/disconnect", HTTP_DELETE, [this](AsyncWebServerRequest *request) {
        if (_storage.hasCredentials()) {
            _storage.destroyCredentials();
            _network.WIFI_CONNECTION_STATUS = WL_DISCONNECTED;
        }
        request->send(200, "text/json", "{\"status\": \"disconnected\"}");
    });

    _server.on("/restart", HTTP_GET, [this](AsyncWebServerRequest *request) {
        if (_storage.hasCredentials()) {
            ESP.restart();
        }
        request->send(200, "text/json", "{\"status\": \"restarted\"}");
    });

    _server.addHandler(new CaptiveRequestHandler()).setFilter(ON_AP_FILTER);//only when requested from AP
    _server.begin();
}

wl_status_t MyServer::getConnectionStatus() {
    return _network.WIFI_CONNECTION_STATUS;
}

void MyServer::run() {
    _dnsServer.processNextRequest();
}