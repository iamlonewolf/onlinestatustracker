#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <DNSServer.h>
#include <ESPAsyncWebServer.h>
#include <ESPAsyncTCP.h>
#include <WebUtilConstant.h>
#include <Network.h>
#include <Storage.h>
#include <WebUtilConstant.h>
#include <AppConstant.h>
#include <Timey.h>
#include <State.h>
#include <DevConfig.h>
#include <ArduinoJson.h>
#include <ServiceApi.h>

#ifndef MyServer_h
#define MyServer_h

enum MyServerStatus {
    UNAVAILABLE,
    CONNECTING,
    SUCCESS,
    FAILED,
};

class MyServer {
    public:
        MyServer();
        static const byte DNS_PORT = 53;
        static constexpr const char * SSID = "AFKTrackerDevice2";
        
        void initialize(std::function<void(MyServerStatus)> cb);
        void connect(bool async = false);
        void credentials(char* ssid, char *password);
        void initializeServer();
        wl_status_t getConnectionStatus();
        void run();

        static String getRedirectUrl(String path) {
            return "http://" + WiFi.softAPIP().toString() + path;
        }

        String email;
        String password;
    private:
        IPAddress _apId{172, 0, 0, 1};
        IPAddress _subNetMask{255, 255, 255, 0};

        DNSServer _dnsServer; // create DNS server for captive portal
        AsyncWebServer _server{80}; // create server
        Storage _storage;
        Network _network; // network library
        ServiceApi _service;
        String processor(const String& var, String content);
        void handleMainContent(AsyncWebServerRequest *request);
        void handlNetworkContent(AsyncWebServerRequest *request);
        void handleStatusListContent(AsyncWebServerRequest *request);
        void handleNetworkLogin(AsyncWebServerRequest *request);
        void handleAccountLogin(AsyncWebServerRequest *request);
        void handleAccountLogout(AsyncWebServerRequest *request);
        void handleAccountProfile(AsyncWebServerRequest *request);
        void handlePOSTToken(AsyncWebServerRequest *request);
        void handleGETToken(AsyncWebServerRequest *request);
        void handleLoginWithToken(AsyncWebServerRequest *request);

};

class CaptiveRequestHandler : public AsyncWebHandler {
    public:
        CaptiveRequestHandler() {}
        virtual ~CaptiveRequestHandler() {}

        bool canHandle(AsyncWebServerRequest *request){
            return true;
        }

        void handleRequest(AsyncWebServerRequest *request) {
            request->redirect(MyServer::getRedirectUrl("/checkLogin"));
        }
};

#endif
