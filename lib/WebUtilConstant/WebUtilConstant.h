#include <Arduino.h>
#ifndef WebUtilConstant_h
#define WebUtilConstant_h


const char NETWORK_LOGIN_CONTENT[] PROGMEM = R"rawliteral(
<main id="main" class="box-wrap">
  <form id="login-form" 
    method="POST" 
    class="position-relative"
    onsubmit="event.preventDefault(); submitNetworkLogin()"
    >
    <div id="loader" class="loader d-none">
      <div class="d-flex justify-content-center align-items-center h-100">
        <div class="lds-ripple">
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
    <h1 class="h3 mb-3 fw-bold">AFK Tracker</h1>   
    <h5 class="mb-2">(WiFI Login)</h5> 
    <div class="login-form">
      <div class="form-floating">
        <input type="text" id="ssid" readonly="true" class="form-control fw-bold" value="%SSID%">
        <label for="ssid">Network Name(SSID)</label>
      </div>
      <div class="form-floating">
        <input type="password" class="form-control" id="password" required>
        <label for="password">Network Key</label>
      </div>
      <button class="w-100 btn btn-lg btn-primary mb-2" type="submit">Submit</button>
    </div>
    <a href="/networks" class="btn-link">Back</a>
  </form>
</main>
)rawliteral";

const char ACCOUNT_LOGIN_CONTENT[] PROGMEM = R"rawliteral(
<main id="main" class="box-wrap">
  <form id="login-form" 
    method="POST" 
    class="position-relative"
    onsubmit="event.preventDefault(); submitAccountLogin()"
    >
    <div id="loader" class="loader d-none">
      <div class="d-flex justify-content-center align-items-center h-100">
        <div class="lds-ripple">
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
    <h1 class="h3 mb-3 fw-bold">AFK Tracker</h1>   
    <h5 class="mb-2">(Account Login)</h5> 
    <div class="login-form">
      <div class="form-floating">
        <input type="text" id="email" class="form-control fw-bold" />
        <label for="email">Email</label>
      </div>
      <div class="form-floating">
        <input type="password" class="form-control" id="password" required>
        <label for="password">Password</label>
      </div>
      <button class="w-100 btn btn-lg btn-primary mb-2" type="submit">Submit</button>
    </div>
    <a href="/main" class="btn-link">Go back</a>
  </form>
</main>
)rawliteral";

const char NETWORK_LIST_CONTENT[] PROGMEM = R"rawliteral(
<main class="box-wrap position-relative">
  <div id="loader" class="loader d-none">
    <div class="d-flex justify-content-center align-items-center h-100">
      <div class="lds-ripple">
        <div></div>
        <div></div>
      </div>
    </div>
  </div>
  <h1 class="h3 mb-3 fw-bold">AFK Tracker</h1>
  <span class="mb-2 fw-normal d-block">(Select network to connect your device)</span>
  <ul id="ssid-list" class="list-group text-start mb-2"></ul>
  <div id="network-message" 
    class="alert alert-light border-danger text-danger p-2">
      No network found.
  </div>
  <a id="refresh-network-btn" class="btn-link mb-2" onclick="loadNetworks()">Refresh network</a>
  <div class="mt-2">
    <a href="/main" id="back-btn" class="btn-link d-none">Back to main</a>
  </div>
</main>

<script>
(function(){
  loadNetworks();
}());
</script>
)rawliteral";

const char NETWORK_CONFIGURED_CONTENT[] PROGMEM = R"rawliteral(
<main class="box-wrap position-relative">
  <h1 class="h3 mb-3 fw-bold">AFK Tracker</h1>
  <p class="mb-2 fw-normal d-block">Device connection credentials is set. 
  White light indicator will be turned on indicating that your device is successfully connected to your network.</p>
  <p id="processing-text" class="processing-text d-none instruction-text">Connecting to network..</p>
  <p id="login-instruction-text" class="d-none instruction-text">Connection successful. Click <a href="/accountLogin" class="btn-link">here</a> to login your account.</p>
  <p id="failed-network-login-text" class="d-none instruction-text">There is problem in your connection. Please check your credential settings. Click <a href="/networks" class="btn-link">here</a> to select a network.</p>
</main>
<script>
(function(){
    loadCheckNetworkLoginStatus();
}());
</script>
)rawliteral";

const char STATUS_LIST_CONTENT[] PROGMEM = R"rawliteral(
<main class="box-wrap position-relative">
  <h1 class="h3 mb-3 fw-bold">AFK Tracker</h1>
  <div id="status-list">
    <button onclick="setStatus(CONNECTION_STATE_LABEL_ACTIVE)" class="btn btn-status active">ACTIVE</button>
    <button onclick="setStatus(CONNECTION_STATE_LABEL_BUSY)" class="btn btn-status busy">BUSY</button>
    <button onclick="setStatus(CONNECTION_STATE_LABEL_AWAY)" class="btn btn-status away">AWAY</button>
    <!--<button onclick="setStatus(CONNECTION_STATE_LABEL_INACTIVE)" class="btn btn-status inactive">INACTIVE</button>-->
    <button onclick="setStatus(CONNECTION_STATE_LABEL_OFF)" class="btn btn-status off">OFFLINE</button>
    <div class="mt-2">
      <a href="/main" id="back-btn" class="btn-link d-none">Back to main</a>
    </div>
  </div>
</main>
<script>
loadStatusListContent();
</script>
)rawliteral";

const char MAIN_CONTENT[] PROGMEM = R"rawliteral(
<main class="box-wrap position-relative">
  <h1 class="h3 mb-3 fw-bold">AFK Tracker</h1>
  <div id="status-list">
    <a href="/setStatus" class="btn">SET STATUS</a>
    <a href="/accountProfile" class="btn">ACCOUNT</a>
    <a href="/networks" class="btn">NETWORK</a>
    <a onclick="disconnect()" class="btn-link">Disconnect</a>
  </div>
</main>
)rawliteral";

const char ACCOUNT_PROFILE_CONTENT[] PROGMEM = R"rawliteral(
<main class="box-wrap position-relative">
  <h1 class="h3 mb-3 fw-bold">AFK Tracker</h1>
  <h5 class="mb-2">(Account Profile)</h5> 
  <div class="mb-2 text-left">
    <label class="fw-bold">First Name:</label>
    <input type="text" id="first-name" class="form-control" disabled />
  </div>
  <div class="mb-3 text-left">
    <label class="fw-bold">Last Name:</label>
    <input type="text" id="last-name" class="form-control" disabled />
  </div>
  <a href="/accountLogout?redirect=/accountLogin" class="d-block btn mb-2">Login other account</a>
  <a href="/accountLogout" class="d-block btn mb-2">Logout</a>
  <a href="/main" class="d-block btn">Go back</a>
</main>
<script>
loadAccountProfile();
</script>
)rawliteral";


#endif
