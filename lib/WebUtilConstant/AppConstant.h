#include <Arduino.h>
#ifndef AppConstant_h
#define AppConstant_h

const String CONNECTION_STATE_LABEL_ACTIVE = "active";
const String CONNECTION_STATE_LABEL_INACTIVE = "inactive";
const String CONNECTION_STATE_LABEL_BUSY = "busy";
const String CONNECTION_STATE_LABEL_AWAY = "away";
const String CONNECTION_STATE_LABEL_OFF = "off";

#endif