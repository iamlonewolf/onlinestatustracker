#include <Arduino.h>
#ifndef Timey_h
#define Timey_h

using  TimeyCb = std::function<void()>;
using  TimeyBoolCb = std::function<bool(unsigned long)>;

struct TimeySettings {
    unsigned long interval;
    unsigned long baseTime;
    TimeyCb cb; 
    TimeyCb reset;
};

class Timey {
    public:
        Timey();
        static const unsigned long TIME_0 = 0;
        static const unsigned long MILLISECOND_1 = 1;
        static const unsigned long MILLISECOND_40 = 40;
        static const unsigned long MILLISECOND_50 = 50;
        static const unsigned long MILLISECOND_100 = 100;
        static const unsigned long MILLISECOND_300 = 300;
        static const unsigned long MILLISECOND_500 = 500;
        static const unsigned long MILLISECOND_600 = 600;
        static const unsigned long MILLISECOND_700 = 700;
        static const unsigned long SECOND_1 = 1000;
        static const unsigned long SECOND_2 = 2000;
        static const unsigned long SECOND_3 = 3000;
        static const unsigned long SECOND_4 = 4000;
        static const unsigned long SECOND_5 = 5000;
        static const unsigned long SECOND_6 = 6000;
        static const unsigned long SECOND_7 = 7000;
        static const unsigned long SECOND_8 = 8000;
        static const unsigned long SECOND_9 = 9000;
        static const unsigned long SECOND_10 = 10000;
        static const unsigned long SECOND_11 = 11000;
        static const unsigned long SECOND_12 = 12000;
        static const unsigned long SECOND_13 = 13000;
        static const unsigned long SECOND_14 = 14000;
        static const unsigned long SECOND_15 = 15000;
        static const unsigned long SECOND_20 = 20000;
        static const unsigned long SECOND_30 = 30000;
        static const unsigned long SECOND_40 = 40000;
        static const unsigned long SECOND_50 = 50000;
        static const unsigned long MINUTE_1 = 60000;
        static const unsigned long MINUTE_2 = 120000;

        static std::vector<TimeySettings> intervalData;
        static TimeySettings generate(unsigned long interval = Timey::TIME_0)  {
            TimeySettings settings;
            settings.interval = interval;
            settings.baseTime = 0;
            return settings;
        }

        static void createInterval(unsigned long interval, TimeyCb func) {
            TimeySettings settings = generate(interval);
            settings.cb = func;
            intervalData.push_back(settings);
        }

        static TimeyBoolCb createToggle() {
            TimeySettings settings = generate();
            bool toggle = true;
            return [settings, toggle](unsigned long interval = Timey::TIME_0) mutable {
                if (interval > Timey::TIME_0) {
                    settings.interval = interval;
                }
                if (millis() > (settings.baseTime + interval)) {
                    toggle = !toggle;
                    settings.baseTime = millis();
                }
                return toggle;
            };
        }
        
        static TimeyBoolCb createTimeout() {
            TimeySettings settings = generate();
            unsigned long lastElapsedTime = Timey::TIME_0;
            bool locked = false;
            return [settings, locked, lastElapsedTime](unsigned long interval = Timey::TIME_0) mutable {
                if (interval > Timey::TIME_0) {
                    settings.interval = interval;
                }
                if (lastElapsedTime == Timey::TIME_0) {
                    settings.baseTime = millis();
                }
                if (millis() - lastElapsedTime > Timey::MILLISECOND_40) {
                    settings.baseTime = millis();
                    // Serial.printf("reset interval %6ld\n", interval);    
                    locked = false;
                }
                // Serial.printf("millis = %6ld, lastElapsedTime = %6ld interval = %6ld\n", millis(), lastElapsedTime, interval);
                lastElapsedTime = millis();
                if (millis() > settings.baseTime + settings.interval && !locked) {
                    locked = true;
                    return true;
                }
                return false;
            };
        }

        static void run() {
            runInterval();
        }
         
    private:
        static void runInterval() {
            for(std::vector<TimeySettings>::iterator it = intervalData.begin(); it != intervalData.end(); ++it) {
                if (millis() > (it->baseTime + it->interval)) {
                    it->cb();
                    it->baseTime = millis();
                }
            }
        }
};

#endif
