#include <DevConfig.h>

const String DevConfig::DEVICE_ID = "afktrackerdev2";
const String DevConfig::PROD_API_URL = "http://service3.iot.johnsikstri.com/api";
// const String DevConfig::LOCAL_API_URL = "https://service1.iot.johnsikstri.com/api";
const String DevConfig::LOCAL_API_URL = "http://192.168.1.2:3003/api";
// const String DevConfig::LOCAL_API_URL = "http://172.0.0.100:3003/api";
// const String DevConfig::LOCAL_API_URL = "http://192.168.254.104:3003/api";
// const String DevConfig::LOCAL_API_URL = "http://localhost:3003/api";
// const String DevConfig::LOCAL_API_URL = "http://192.168.1.6:3003/api";
// const String DevConfig::LOCAL_API_URL = "http://192.168.1.6:3003/api";
// const String DevConfig::LOCAL_API_URL = "http://169.254.196.182:3003/api";   
DevEnvironment DevConfig::_environment;
