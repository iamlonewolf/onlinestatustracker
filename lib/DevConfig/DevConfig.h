#include <Arduino.h>

#ifndef DevConfig_h
#define DevConfig_h

enum DevEnvironment {
    LOCAL_ENV,
    PRODUCTION_ENV,
};

class DevConfig {
    public:
        DevConfig();
        static const String DEVICE_ID;
        static const String PROD_API_URL;
        static const String LOCAL_API_URL;

        static void setEnvironment(DevEnvironment environment) {
            _environment = environment;
        }

        static DevEnvironment getEnvironment() {
            return _environment;
        }

        static const String getApiUrl() {
            if (DevConfig::getEnvironment() == DevEnvironment::PRODUCTION_ENV) {
                return DevConfig::PROD_API_URL;
            }
            return DevConfig::LOCAL_API_URL;
        }

    private:
        static DevEnvironment _environment;
};

#endif  