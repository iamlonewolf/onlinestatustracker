
#include <Arduino.h>
#include <U8g2lib.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <Timey.h>
#include <UltraSonic.h>
#include <MyServer.h>
#include <Servow.h>
#include <View.h>
#include <Control.h>
#include <State.h>
#include <ServiceApi.h>
#include <DataLogger.h>
#include <DevConfig.h>

class App {
    public:
        App(U8G2* u8g2);
        static const unsigned long BAUD_RATE;
        // assigned pins
        static const uint8_t TRIGGER_PIN;
        static const uint8_t ECHO_PIN;
        static const uint8_t SERVO_PIN;

        static const uint8_t LED_SUCCESS;
        static const uint8_t LED_DANGER;
        static const uint8_t LED_WARNING;

        static const uint8_t LED_DEFAULT;

        unsigned int MAX_RANGE = 19;

        void initialize();

        void run(); // run app in loop
        void watch();
        void watchActive();
        void viewActive();
        void watchDetect();
        void watchRotate();
        void watchInactive();
        void watchAway();
        void watchBusy();
        void watchOff();
        void watchLogout();
        void watchAccountLogin();
        void viewInactive();
        void viewAway();
        void viewBusy();
        void viewOff();
        void watchScan();
        void doDetect();
        void doRotate();

        void alertScan();

    private:
        U8G2* _u8g2;
        UltraSonic _sonic{App::TRIGGER_PIN, App::ECHO_PIN};
        Servow _servow{App::SERVO_PIN};
        View _view{_u8g2};
        MyServer _server;
        DataLogger _dataLogger;
        Storage _storage;
        ServiceApi _service;
        
        TimeyBoolCb rangeCheckInterval = Timey::createToggle();
        TimeyBoolCb rangeTimeout = Timey::createTimeout();

        TimeyBoolCb scanAreaToggle = Timey::createToggle();
        TimeyBoolCb blinkerToggle = Timey::createToggle();
        TimeyBoolCb scanningTimeout = Timey::createTimeout();
        TimeyBoolCb servoRotationTimeout = Timey::createTimeout();
        TimeyBoolCb scanTimeout = Timey::createTimeout();
        TimeyBoolCb awayTimeout = Timey::createTimeout();
        TimeyBoolCb activeTimeout = Timey::createTimeout();
        TimeyBoolCb busyTimeout = Timey::createTimeout();
        TimeyBoolCb inactiveTimeout = Timey::createTimeout();
        TimeyBoolCb logoutTimeout = Timey::createTimeout();
        TimeyBoolCb alertScanToggle = Timey::createToggle();

        Control _controlSet1;
        Control _controlSet2;

        int rotationCounter = 1;
        bool increaseCounter = true;

        void initializeServo();
        void initializePins();
        void initializeTimings();
        void initializeBootupMessage();
        void initializeServer();
        void initializeServoCheck();

        void wifiConnectionMessage(MyServerStatus status);
        void wifiConnectionLight(MyServerStatus status);

        void logStatusActive();
        void logStatusInactive();
        void logStatusAway();
        void logStatusBusy();
        void logStatusOff();

        void watchWifiStatus();
};
