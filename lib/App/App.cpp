#include <App.h>

const unsigned long App::BAUD_RATE = 921600;
const uint8_t App::TRIGGER_PIN = 12;
const uint8_t App::ECHO_PIN = 14;
const uint8_t App::SERVO_PIN = 2;
const uint8_t App::LED_SUCCESS = 13;
const uint8_t App::LED_DANGER = 10;
const uint8_t App::LED_WARNING = 15;
const uint8_t App::LED_DEFAULT = 16;


/**
 * ============
 * PINOUT
 * ============ 
 * RED     PIN=10
 * GREEN   PIN=13
 * BLUE    PIN=15
*/
App::App(U8G2* u8g2) {
    _u8g2 = u8g2;
    _u8g2->setDisplayRotation(U8G2_R2);
    _view.initialize(_u8g2);    
    _sonic.setMaxRange(App::MAX_RANGE);
    DevConfig::setEnvironment(DevEnvironment::PRODUCTION_ENV);
    initializeServo();
    initializePins();
};

void App::initializeServo() {
    _servow.setDegree(Servow::MAX_DEGREE);
    _servow.setNumRotation(Servow::ROTATE_6);
}

void App::initializePins() {
    // control batch 1
    std::vector<ControlSetttings> controls1;
    ControlSetttings ledSuccess;
    ledSuccess.pin = App::LED_SUCCESS;
    ledSuccess.mode = OUTPUT;

    ControlSetttings ledDanger;
    ledDanger.pin = App::LED_DANGER;
    ledDanger.mode = OUTPUT;

    ControlSetttings ledWarning;
    ledWarning.pin = App::LED_WARNING;
    ledWarning.mode = OUTPUT;

    controls1.push_back(ledSuccess);
    controls1.push_back(ledDanger);
    controls1.push_back(ledWarning);
    _controlSet1.initialize(controls1);

    // control batch 2
    ControlSetttings ledDefault;
    ledDefault.pin = App::LED_DEFAULT;
    ledDefault.mode = OUTPUT;
    std::vector<ControlSetttings> controls2;
    controls2.push_back(ledDefault);
    _controlSet2.initialize(controls2);
}

void App::viewActive() {
    _view.activeText();
    _view.drawBorder();
    _controlSet1.toggleAllExcept(App::LED_SUCCESS, HIGH);    
}

void App::watchActive() {
    viewActive();
    if (_sonic.getInch() > _sonic.getMaxRange()) {
        if (rangeTimeout(3000)) { //3000
            State::setScanState(ScanStateMode::DETECT);
            State::setAppState(AppStateMode::SCAN);
        }
    }
    _sonic.detect();
}

void App::logStatusActive() {
    viewActive();
    if (activeTimeout(Timey::MILLISECOND_100)) {
        _dataLogger.postStatusActive();
        State::setAppState(AppStateMode::ACTIVE);
    }
}

void App::viewInactive() {
    _view.inactiveText();
    _view.drawBorder();
    // _controlSet1.toggleAllExcept(App::LED_WARNING, HIGH);
    _controlSet1.toggleOn(App::LED_SUCCESS);
    _controlSet1.toggleOn(App::LED_DANGER);
    _controlSet1.toggleOff(App::LED_WARNING);
}

void App::watchInactive() {
    viewInactive();
    if (_sonic.getInch() < _sonic.getMaxRange()) {
        State::setAppState(AppStateMode::LOG_ACTIVE);
    }

    if (awayTimeout(10000)) {
        State::setAppState(AppStateMode::LOG_AWAY);
    }
    _sonic.detect();
}

void App::logStatusInactive() {
    viewInactive();
    if (activeTimeout(Timey::MILLISECOND_100)) {
        _dataLogger.postStatusInactive();
        State::setAppState(AppStateMode::INACTIVE);
    }
}

void App::viewAway() {
    _view.awayText();
    _view.drawBorder();
    // _controlSet1.toggleAllExcept(App::LED_WARNING, HIGH);

    _controlSet1.toggleOn(App::LED_SUCCESS);
    _controlSet1.toggleOn(App::LED_DANGER);
    _controlSet1.toggleOff(App::LED_WARNING);
}

void App::watchAway() {
    viewAway();
    if (_sonic.getInch() < _sonic.getMaxRange() && !State::isUserInitiated()) {
        State::setAppState(AppStateMode::LOG_ACTIVE);
    }
    _sonic.detect();
}

void App::logStatusAway() {
    viewAway();
    if (activeTimeout(Timey::MILLISECOND_100)) {
        _dataLogger.postStatusAway();
        State::setAppState(AppStateMode::AWAY);
    }
}

void App::viewBusy() {
    _view.busyText();
    _view.drawBorder();
    _controlSet1.toggleAllExcept(App::LED_DANGER, HIGH);
}

void App::watchBusy() {
    viewBusy();
}

void App::logStatusBusy() {
    viewBusy();
    if (busyTimeout(Timey::MILLISECOND_100)) {
        _dataLogger.postStatusBusy();
        State::setAppState(AppStateMode::BUSY);
    }
}

void App::viewOff() {
    _view.offlineText();
    _view.drawBorder();
    _controlSet1.toggleOff(App::LED_SUCCESS);
    _controlSet1.toggleOff(App::LED_DANGER);
    _controlSet1.toggleOff(App::LED_WARNING);
}

void App::watchOff() {
    viewOff();
}

void App::logStatusOff() {
    viewOff();
    if (busyTimeout(Timey::MILLISECOND_100)) {
        _dataLogger.postStatusOff();
        State::setAppState(AppStateMode::OFF);
    }
}

void App::watchLogout() {
    _view.logoutText();
    if (logoutTimeout(Timey::MILLISECOND_500)) {
        _dataLogger.postStatusOff();
        _storage.destroyToken();
        State::setAppState(AppStateMode::ACTIVE);
    }
}

void App::watchAccountLogin() {
    _controlSet1.toggleAllOff();
    _view.loggingInText();

    Serial.printf("EMAIL: %s, LENGTH %d\n", _server.email.c_str(), _server.email.length());
    Serial.printf("PASSWORD: %s, LENGTH %d\n", _server.password.c_str(), _server.password.length());

    Serial.println("sending post active status started");
    const String endpoint = DevConfig::getApiUrl() + ServiceApi::ENDPOINT_IDENTITY_LOGIN;
    String payload;
    StaticJsonDocument<100> doc;
    doc["email"] = _server.email.c_str();
    doc["password"] = _server.password.c_str();
    serializeJson(doc, payload);
    State::setAccountLoginState(AccountLoginStateMode::PROCESSING);
    _service.post(endpoint, payload,  [this](HTTPClient *http, int httpCode) {
        Serial.println(http->header("authorization"));
        Serial.println("sending post identity login status success");
        if (httpCode == 200) {
            State::setAccountLoginState(AccountLoginStateMode::SUCCESS);
            State::setAppState(AppStateMode::LOG_ACTIVE);
            _storage.setToken(http->header("authorization"));
        } else {
            State::setAccountLoginState(AccountLoginStateMode::FAILED);
            State::setAppState(AppStateMode::ACTIVE);
        }
    }, [this](int httpCode, String error) {
        State::setAppState(AppStateMode::ACTIVE);
        State::setAccountLoginState(AccountLoginStateMode::FAILED);
        Serial.println("sending post identity login status failed");
    });

    
}

void App::doDetect() {
    _sonic.detect();
    if (scanningTimeout(Timey::MILLISECOND_500)) { //400
        if (_sonic.getInch() > _sonic.getMaxRange()) {
            State::setScanState(ScanStateMode::ROTATE);
        } else {
            State::setScanState(ScanStateMode::STANDBY);
            logStatusActive();
        }
    }
}

void App::doRotate() {
    int degreeOfRotation = _servow.getDegreeRotation() * rotationCounter;
    _servow.rotate(degreeOfRotation);
    // Serial.printf("DO ROTATE... %d\n", degreeOfRotation);
    if (servoRotationTimeout(Timey::MILLISECOND_300)) { //700
        if (increaseCounter) {
            rotationCounter++;
        } else {
            rotationCounter--;
        }
        if (rotationCounter >= _servow.getNumRotation()) {
            increaseCounter = false;
        }
        if (rotationCounter <= 1) {
            increaseCounter = true;
        }
        State::setScanState(ScanStateMode::DETECT);
        // Serial.println("do scan now...");
    }
}

void App::watchScan() {
    alertScan();
    switch (State::getScanState()) {
        case ScanStateMode::DETECT:
            doDetect();
            break;
        case ScanStateMode::ROTATE:
            doRotate();
            break;
        default:
            State::setAppState(AppStateMode::ACTIVE);
    }

    if (inactiveTimeout(Timey::SECOND_15)) { // Timey::SECOND_6
        State::setAppState(AppStateMode::LOG_INACTIVE);
        _servow.rotate(Servow::DEGREE_90);
    }
}

void App::watch() {
    switch (State::getAppState()) {
        case AppStateMode::LOG_ACTIVE:
            logStatusActive();
            break;
        case AppStateMode::LOG_INACTIVE:
            logStatusInactive();
            break;
        case AppStateMode::LOG_AWAY:
            logStatusAway();
            break;
        case AppStateMode::LOG_BUSY:
            logStatusBusy();
            break;
        case AppStateMode::LOG_OFF:
            logStatusOff();
            break;
        case AppStateMode::SCAN:
            watchScan();
            break;
        case AppStateMode::INACTIVE:
            watchInactive();
            break;
        case AppStateMode::AWAY:
            watchAway();
            break;
        case AppStateMode::BUSY:
            watchBusy();
            break;
        case AppStateMode::OFF:
            watchOff();
            break;
        case AppStateMode::LOGOUT:
            watchLogout();
            break;
        case AppStateMode::ACCOUNT_LOGIN:
            watchAccountLogin();
            break;
        case AppStateMode::ACTIVE:
            watchActive();
    }
}

void App::alertScan() {
    bool toggle = alertScanToggle(Timey::MILLISECOND_500);
    _view.scanningText(toggle);
}

void App::initializeTimings() {
    Serial.println("initializing watch status timer..");
    Timey::createInterval(Timey::MILLISECOND_1, [this]() {
        watch();
    });
    
    Timey::createInterval(Timey::MILLISECOND_1, [this]() {
        watchWifiStatus();
    });
}

void App::initializeBootupMessage() {
    Serial.println("initializing welcome message..");
    _view.welcomeMessageText();
    // delay(Timey::SECOND_2);
}

void App::initializeServoCheck() {
    Serial.println("initializing servo rotation check..");
    _servow.rotate(Servow::DEGREE_0);
    delay(Timey::SECOND_1); 
    _servow.rotate(Servow::DEGREE_180);
    delay(Timey::SECOND_1);
    _servow.rotate(Servow::DEGREE_90);
    delay(Timey::SECOND_1);
}

void App::initializeServer() {
    Serial.println("initializing server setup..");
    _server.initialize([this](MyServerStatus status) {
        if (status != MyServerStatus::UNAVAILABLE) {
            wifiConnectionMessage(status);
            wifiConnectionLight(status);
        }
    });
    _server.initializeServer();
}

void App::watchWifiStatus() {
    _controlSet2.toggle(App::LED_DEFAULT, _server.getConnectionStatus() == WL_CONNECTED ? HIGH : LOW);
    ServiceApi::setConnectionStatus(_server.getConnectionStatus());
}

void App::wifiConnectionMessage(MyServerStatus status) {
    switch (status) {
        case MyServerStatus::CONNECTING:
            Serial.println("connecting to network..");
            _view.wifiConnectingText();
            break;
        case MyServerStatus::SUCCESS:
            Serial.println("connection to network success.");
            Serial.println(WiFi.localIP());
            _view.wifiConnectionSuccessText();
            break;
        case MyServerStatus::FAILED:
            Serial.println("connection to network failed.");
            _view.wifiConnectionFailedText();
            break;
    }
}

void App::wifiConnectionLight(MyServerStatus status) {
    unsigned long BLINK_DELAY = 100;
    int MAX_BLINK = 7;
    bool toggleMode = true;
    for (int i = 0; i < MAX_BLINK; i++) {
        if (status == MyServerStatus::SUCCESS) {
            _controlSet1.toggleAllExcept(App::LED_SUCCESS, toggleMode ? HIGH : LOW);
        }

        if (status == MyServerStatus::FAILED) {
            _controlSet1.toggleAllExcept(App::LED_DANGER, toggleMode ? HIGH : LOW);
        }

        toggleMode = !toggleMode;
        delay(BLINK_DELAY);
    }
    delay(Timey::MILLISECOND_700);
}

void App::initialize() {
    initializeTimings();
    initializeBootupMessage();
    initializeServoCheck();
    initializeServer();
    Serial.println("initialize done. device started.");
    logStatusActive();
}

void App::run() {
    Timey::run();
    _server.run();
}

