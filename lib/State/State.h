#include <Arduino.h>
#include <AppConstant.h>

#ifndef State_h
#define State_h

enum class UserStateMode {
    ACTIVE,
    BUSY,
    AWAY,
    HIDDEN,
};

enum class AppStateMode {
    SCAN,
    ACTIVE,
    INACTIVE,
    AWAY,
    BUSY,
    OFF,
    LOG_ACTIVE,
    LOG_INACTIVE,
    LOG_AWAY,
    LOG_BUSY,
    LOG_OFF,
    ACCOUNT_LOGIN,
    LOGOUT,
};

enum class ScanStateMode {
    DETECT,
    ROTATE,
    STANDBY,
};

enum class AccountLoginStateMode {
    PROCESSING,
    FAILED,
    SUCCESS,
};

class State {
    public:
        State();
        static void setAppState(AppStateMode state) {
            _appState = state;
        }

        static AppStateMode getAppState() {
            return _appState;
        }

        static String getAppStateText() {
            switch (State::getAppState()) {
                case AppStateMode::ACTIVE:
                    return CONNECTION_STATE_LABEL_ACTIVE;
                case AppStateMode::INACTIVE:
                    return CONNECTION_STATE_LABEL_INACTIVE;
                case AppStateMode::BUSY:
                    return CONNECTION_STATE_LABEL_BUSY;
                case AppStateMode::AWAY:
                    return CONNECTION_STATE_LABEL_AWAY;
                case AppStateMode::OFF:
                    return CONNECTION_STATE_LABEL_OFF;
                default:
                    return CONNECTION_STATE_LABEL_ACTIVE;
            }
        }

        static void setUserState(UserStateMode state) {
            _userState = state;
        }

        static UserStateMode getUserState() {
            return _userState;
        }

        static void setScanState(ScanStateMode state) {
            _scanState = state;
        }

        static ScanStateMode getScanState() {
            return _scanState;
        }

        static void setAccountLoginState(AccountLoginStateMode state) {
            _accountLoginState = state;
        }

        static AccountLoginStateMode getAccountLoginState() {
            return _accountLoginState;
        }

        static String getAccountLoginStateToString() {
            switch (getAccountLoginState()) {
                case AccountLoginStateMode::SUCCESS:
                    return "success";
                case AccountLoginStateMode::FAILED:
                    return "failed";
                default:
                    return "processing";
            }
        }

        static void setUserInitiated(bool userInitiated) {
            _userInitiated = userInitiated;
        }

        static bool isUserInitiated() {
            return _userInitiated;
        }

    private:
        static AppStateMode _appState;
        static UserStateMode _userState;
        static ScanStateMode _scanState;
        static AccountLoginStateMode _accountLoginState;
        static bool _userInitiated;
};

#endif