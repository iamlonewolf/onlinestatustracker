#include <State.h>
AppStateMode State::_appState = AppStateMode::LOG_ACTIVE;
UserStateMode State::_userState = UserStateMode::ACTIVE;
ScanStateMode State::_scanState = ScanStateMode::STANDBY;
AccountLoginStateMode State::_accountLoginState = AccountLoginStateMode::PROCESSING;
bool State::_userInitiated = false;