const ENC_TYPE_NONE = 4;
const CONNECTION_STATE_LABEL_ACTIVE = "active";
const CONNECTION_STATE_LABEL_INACTIVE = "inactive";
const CONNECTION_STATE_LABEL_AWAY = "away";
const CONNECTION_STATE_LABEL_BUSY = "busy";
const CONNECTION_STATE_LABEL_OFF = "off";

const KEY_AUTHORIZATION = "authorization";

async function getAvailableNetworks() {
  let response = await fetch("/scan");
  return response.json();
}

async function initializeRestart() {
  let response = await fetch("/restart");
  return response.json();
}

async function initializeConnect() {
  let response = await fetch("/connect");
  return response.json();
}

async function checkNetworkLoginStatus() {
  let response = await fetch("/checkNetworkLoginStatus");
  return response.json();
}

async function submitToken(token) {
  let params = new FormData();
  params.append("token", token);
  return postRequest("/token", params)
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log(error);
      showAlertMessage();
    });
}

async function loginWithToken(token) {
  let params = new FormData();
  params.append("token", token);
  return postRequest("/login/token", params)
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log(error);
      showAlertMessage();
    });
}

async function submitAccountLogout() {
  return postRequest("/accountLogout")
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log(error);
      showAlertMessage();
    });
}

function loadNetworks() {
  const MAX_RETRY = 10;
  let retryConnectionCounter = 0;
  const message = getNetworkMessageElement();
  const refreshNetworkButton = getRefreshNetworkButtonElement();
  const loader = getLoaderElement();
  const ssidElement = getSsidListElement();

  const init = () => {
    message?.classList.add("d-none");
    refreshNetworkButton?.classList?.add("d-none");
    showLoader();
    getAvailableNetworks()
      .then((data) => {
        console.log("RESULT:", retryConnectionCounter);
        if (!data?.length && retryConnectionCounter < MAX_RETRY) {
          setTimeout(() => {
            init();
            retryConnectionCounter++;
          }, 1500);
          return;
        }
        retryConnectionCounter = 0;
        if (data?.length > 0) {
          message?.classList.add("d-none");
          ssidElement.classList.remove("d-none");
          ssidElement.innerHTML = data
            ?.map((data) => {
              return `<a href="/networkLogin?ssid=${
                data?.ssid
              }" class="list-group-item list-group-item-action d-flex justify-content-between">
                        <span>${data?.ssid}</span>
                    ${
                      data?.secure != ENC_TYPE_NONE
                        ? "<span>&#128274;</span>"
                        : ""
                    }</a>`;
            })
            ?.join("");
        } else {
          message?.classList.remove("d-none");
          ssidElement.classList.add("d-none");
        }
      })
      .catch(() => {
        message?.classList.remove("d-none");
        ssidElement?.classList.add("d-none");
      })
      .finally(() => {
        refreshNetworkButton?.classList?.remove("d-none");
        hideLoader();
      });
  };

  init();
  if (WIFI_CONNECTED) {
    getBackButtonElement().classList.remove("d-none");
  }
}

function loadCheckNetworkLoginStatus() {
  const MAX_RETRY = 10;
  const INTERVAL = 2000;
  const processingText = getProcessingTextElement();
  const loginInstructionText = getLoginInstructionTextElement();
  const failedNetworkLoginText = getFailedNetworkLoginTextElement();
  let retryCounter = 0;
  let connected = false;
  processingText?.classList?.remove("d-none");
  loginInstructionText?.classList?.add("d-none");
  failedNetworkLoginText?.classList?.add("d-none");
  const init = () => {
    checkNetworkLoginStatus()
      .then((data) => {
        connected = data?.status == "success" ? true : false;
        return data;
      })
      .finally((data) => {
        if (retryCounter < MAX_RETRY && !connected) {
          setTimeout(() => {
            init();
            retryCounter++;
          }, INTERVAL);
          return;
        }
        retryCounter = 0;
        processingText?.classList?.add("d-none");
        if (connected) {
          loginInstructionText?.classList.remove("d-none");
        } else {
          failedNetworkLoginText?.classList.remove("d-none");
        }
      });
  };
  initializeConnect().finally(() => {
    init();
  });
}

function loadStatusListContent() {
  if (WIFI_CONNECTED) {
    getBackButtonElement()?.classList.remove("d-none");
  }

  getRequest("/status")
    .then((response) => {
      return response.json();
    })
    .then((response) => {
      setButtonStatus(response?.status ?? CONNECTION_STATE_LABEL_ACTIVE);
    });
}

function loadAccountProfile() {
  const firstNameElement = getFirstNameElement();
  const lastNameElement = getLastNameElement();
  getRequest("/token", {}).then((response) => {
    const jwt = response?.headers?.get(KEY_AUTHORIZATION);
    if (!jwt) {
      window.location.href = "/accountLogin";
      return;
    }
    const decoded = parseJwt(jwt);
    firstNameElement.value = decoded?.firstName;
    lastNameElement.value = decoded?.lastName;
  });
}

function restartDevice() {
  let countdown = 3;
  const interval = setInterval(() => {
    if (countdown >= 0) {
      getRestartCountdown().innerHTML = `Restarting device in ${countdown}`;
    } else {
      getRestartCountdown().innerHTML = `Device restarted.`;
      initializeRestart().then();
      clearInterval(interval);
    }
    countdown--;
  }, 1000);
}

function submitNetworkLogin() {
  let params = new FormData();
  params.append("ssid", getSsidElement()?.value);
  params.append("password", getPasswordElement()?.value);
  postRequest("/validateNetworkLogin", params)
    .then((response) => {
      return response.json();
    })
    .then((response) => {
      if (response?.message == "ok") {
        window.location.href = "/configured";
      }
    })
    .catch(() => {
      showAlertMessage();
    });
}

function submitAccountLogin() {
  // var headers = new Headers();
  // headers.append("Content-Type", "application/json");
  // const params = JSON.stringify({
  //   email: getEmailElement()?.value,
  //   password: getPasswordElement()?.value,
  // });

  let params = new FormData();
  params.append("email", getEmailElement()?.value);
  params.append("password", getPasswordElement()?.value);

  showLoader();
  postRequest("/validateAccountLogin", params)
    .then(() => {
      return checkAccountLoginStatus();
    })
    .then(() => {
      window.location.href = "/main";
    })
    .finally(() => {
      hideLoader();
    })
    .catch((error) => {
      console.log(error);
      alert("Invalid email or password.");
    });
}

function checkAccountLoginStatus() {
  const RETRY_DELAY = 1000;
  const MAX_RETRY = 5;
  let retryCounter = 0;
  const promise = new Promise(function (resolve, reject) {
    const init = () => {
      if (retryCounter >= MAX_RETRY) {
        retryCounter = 0;
        reject("error");
        return;
      }
      getRequest("/checkAccountLoginStatus")
        .then((response) => {
          return response.json();
        })
        .then(({ message }) => {
          if (!message) {
            reject("error");
            return;
          }
          if (message == "processing") {
            setTimeout(() => {
              init();
              retryCounter++;
            }, RETRY_DELAY);
            return;
          }
          if (message == "success") {
            resolve(message);
          } else {
            reject(message);
          }
        });
    };
    init();
  });
  return promise;
}

function setButtonStatus(status) {
  const elements = getAllButtonStatusElement();
  for (let i = 0; i < elements?.length; i++) {
    const elem = elements[i]?.classList;
    elem.remove("status-active");
    if (elem.contains(status)) {
      elem.add("status-active");
    }
  }
}

function setStatus(status) {
  setButtonStatus(status);
  let formData = new FormData();
  formData.append("status", status);
  postRequest("/status", formData)
    .then()
    .catch(() => {
      showAlertMessage();
    });
}

function showAlertMessage() {
  alert(
    "There is problem in the request. Please check your device connectivity"
  );
}

function disconnect() {
  deleteRequest("/disconnect")
    .then()
    .finally(() => {
      window.location.href = "/networks";
    });
}

function parseJwt(token) {
  try {
    return JSON.parse(atob(token.split(".")[1]));
  } catch (e) {
    return null;
  }
}

function getNetworkMessageElement() {
  return document.getElementById("network-message");
}

function getLoginFormElement() {
  return document.getElementById("login-form");
}

function getRefreshNetworkButtonElement() {
  return document.getElementById("refresh-network-btn");
}

function getLoaderElement() {
  return document.getElementById("loader");
}

function getSsidListElement() {
  return document.getElementById("ssid-list");
}

function getSsidElement() {
  return document.getElementById("ssid");
}

function getEmailElement() {
  return document.getElementById("email");
}

function getPasswordElement() {
  return document.getElementById("password");
}

function getRestartCountdown() {
  return document.getElementById("restart-countdown");
}

function getAllButtonStatusElement() {
  return document.querySelectorAll(".btn-status");
}

function getProcessingTextElement() {
  return document.getElementById("processing-text");
}

function getLoginInstructionTextElement() {
  return document.getElementById("login-instruction-text");
}

function getFailedNetworkLoginTextElement() {
  return document.getElementById("failed-network-login-text");
}

function getBackButtonElement() {
  return document.getElementById("back-btn");
}

function getFirstNameElement() {
  return document.getElementById("first-name");
}

function getLastNameElement() {
  return document.getElementById("last-name");
}

function showLoader() {
  getLoaderElement()?.classList?.remove("d-none");
}

function hideLoader() {
  getLoaderElement()?.classList?.add("d-none");
}

async function postRequest(url = "", postBody = {}, headers = {}) {
  const response = await fetch(url, {
    method: "POST",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    redirect: "follow",
    referrerPolicy: "no-referrer",
    headers: headers,
    body: postBody,
  });
  return response;
}

async function getRequest(url = "", data = {}) {
  const response = await fetch(url, {
    method: "GET",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    redirect: "follow",
    referrerPolicy: "no-referrer",
  });
  return response;
}

async function deleteRequest(url = "", data = {}) {
  const response = await fetch(url, {
    method: "DELETE",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    redirect: "follow",
    referrerPolicy: "no-referrer",
    body: data,
  });
  return response.json();
}
