#include <Arduino.h>
#include <U8g2lib.h>
#include <Wire.h>
#include <App.h>

U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE, /* clock=*/ SCL, /* data=*/ SDA);   // pin remapping with ESP8266 HW I2C
App app(&u8g2);

void setup() {
    Serial.begin(App::BAUD_RATE);
    SPIFFS.begin();
    u8g2.begin();
    app.initialize();
}

void loop() {
    u8g2.firstPage();  
    do {
        app.run();
    } while(u8g2.nextPage());
}